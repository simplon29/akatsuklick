<?php

namespace App\Controller;

use App\Repository\ScoreRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController{
    #[Route('/', 'home.index', methods: ['GET'])]
    public function index(ScoreRepository $scoreRepository): Response {
        $bestScores = $scoreRepository->findBestScoresPerUser();
        $user = $this->getUser();
        $scores = $scoreRepository->findScoresByUser($user);
        
        $totalParties = count($scores);
        $totalScore = array_sum(array_map(function($score) {
            return $score->getScore();
        }, $scores));
    
        $moyenne = ($totalParties > 0) ? $totalScore / $totalParties : 0;
        $rang = $this->determinerRang($moyenne);
        return $this->render('pages/home.html.twig', [
            'bestScores' => $bestScores,
            'totalParties' => $totalParties,
            'moyenne' => $moyenne,
            'rang' => $rang,
        ]);
    }

    private function determinerRang($score) {
        if ($score < 20) {
            return 'Avorton';
        } else if ($score < 40) {
            return 'Genin (débutant)';
        } else if ($score < 60) {
            return 'Chûnin (ninja confirmé)';
        } else if ($score < 80) {
            return 'Jônin (ninja expérimenté)';
        } else if ($score < 100) {
            return 'Kage (Dirigeant)';
        } else if ($score < 120) {
            return 'Maître';
        } else if ($score < 140) {
            return 'Grand-Maître';
        } else if ($score < 160) {
            return 'Kami (Dieu)';
        } else if ($score < 180) {
            return 'Kami des Kamis (Dieu des dieux)';
        } else if ($score < 200) {
            return 'Cheater';
        } else {
            return 'Non classé';
        }
    }
}


