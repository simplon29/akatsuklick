<?php

namespace App\Security;

use App\Entity\Score;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class ScoreVoter extends Voter
{
    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, ['delete'])
            && $subject instanceof Score;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // Si l'utilisateur n'est pas connecté, retournez false
        if (!$user instanceof UserInterface) {
            return false;
        }

        /** @var Score $score */
        $score = $subject;

        switch ($attribute) {
            case 'delete':
                return $this->canDelete($score, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canDelete(Score $score, User $user): bool
    {
        // Si l'utilisateur est propriétaire du score, autorisez la suppression
        return $user === $score->getUser();
    }
}