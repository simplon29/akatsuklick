<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class JeuController extends AbstractController
{
    #[Route('/jeu', name: 'jeu.index')]
    public function index(): Response
    {
        return $this->render('pages/jeu/index.html.twig', [
            'controller_name' => 'JeuController',
        ]);
    }
}
