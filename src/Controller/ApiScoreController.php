<?php

namespace App\Controller;

use App\Entity\Score;
use App\Repository\ScoreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ApiScoreController extends AbstractController
{
    
    #[Route('/apiscore', name: 'apiscore.index')]
    public function index(Request $request, ScoreRepository $scoreRepository, PaginatorInterface $paginator): Response
    {
        $user = $this->getUser();
        $query = $scoreRepository->findBy(['user' => $user], ['id' => 'DESC']);
        $pageNumber = $request->query->getInt('page', 1); // Récupérer le numéro de la page en cours
        $scores = $paginator->paginate($query, $pageNumber, 10);

        return $this->render('pages/apiscore/index.html.twig', [
            'controller_name' => 'ApiScoreController',
            'scores' => $scores,
        ]);
    }

    /**
     * @Route("/apiscore/create", name="apiscore.create", methods={"POST"})
     */
    public function create(Request $request, EntityManagerInterface $manager): Response
    {
        // Récupérez l'utilisateur connecté (si nécessaire)
        $user = $this->getUser();

        // Récupérez le score à partir de la requête
        $data = json_decode($request->getContent(), true);
        $scoreValue = $data['score'];

        // Créez et enregistrez l'entité Score
        $score = new Score();
        $score->setUser($user);
        $score->setScore($scoreValue);
        $score->setCreatedAt(new \DateTimeImmutable());

        $manager->persist($score);
        $manager->flush();

        return new Response('Score enregistré', Response::HTTP_CREATED);
    }

    #[Route('/score/{id}/delete', name: 'score.delete', methods: ['POST', 'GET'])]
    public function delete(Score $score, EntityManagerInterface $entityManager): RedirectResponse
    {
        $user = $this->getUser();

        if (!$user) {
            throw new AccessDeniedException('Vous devez être connecté pour supprimer un score.');
        }
        // Vérifiez si l'utilisateur connecté est autorisé à supprimer le score
        $this->denyAccessUnlessGranted('delete', $score);

        // Supprimez le score et mettez à jour la base de données
        $entityManager->remove($score);
        $entityManager->flush();

        // Ajoutez un message flash pour indiquer que le score a été supprimé
        $this->addFlash('success', 'Partie supprimée avec succès !');

        // Redirigez l'utilisateur vers la liste des scores
        return $this->redirectToRoute('apiscore.index');
    }
}