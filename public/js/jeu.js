document.addEventListener('DOMContentLoaded', function () {
    const bouton = document.getElementById('monBouton');
    const scoreElement = document.getElementById('score');
    const backgroundMusic1 = document.getElementById('backgroundMusic1');
    const backgroundMusic2 = document.getElementById('backgroundMusic2');
    const tempsRestantElement = document.getElementById('tempsRestant');
    let score = 0;
    let tempsRestant = 10;
    let interval;

    function incrementerScore() {
        if (interval) {
            score++;
            scoreElement.innerText = score;
        }
    }

    function demarrerJeu() {
        interval = setInterval(function () {
            tempsRestant--;
            updateTempsRestantDisplay();
            if (tempsRestant <= 0) {
                clearInterval(interval);
                finDuJeu();
            }
        }, 1000);
        playRandomMusic(); // Commence la lecture d'une musique aléatoire
    }

    async function finDuJeu() {
        stopMusic(); // Arrête la lecture de la musique et la rembobine
        alert(`Le jeu est terminé! Votre score est: ${score}`);
        try {
            await enregistrerScore(score);
            window.location.href = "apiscore";
        } catch {
            console.error('Erreur lors de la redirection vers la page "mes scores"');
        }
        reinitialiserJeu();
    }

    function reinitialiserJeu() {
        score = 0;
        scoreElement.innerText = score;
        tempsRestant = 10;
        interval = null;
    }

    function updateTempsRestantDisplay() {
        tempsRestantElement.innerText = tempsRestant;
    }

    bouton.addEventListener('click', () => {
        if (!interval) {
            demarrerJeu();
        }
        incrementerScore();
    });

    function playRandomMusic() {
        const randomMusic = Math.random() < 0.5 ? backgroundMusic1 : backgroundMusic2;
        randomMusic.play().catch((error) => {
            console.error('Erreur lors de la lecture de la musique:', error);
        });
    }


    function stopMusic() {
        backgroundMusic1.pause();
        backgroundMusic1.currentTime = 0;
        backgroundMusic2.pause();
        backgroundMusic2.currentTime = 0;
    }

    function enregistrerScore(score) {
        return new Promise((resolve, reject) => {
            fetch('/apiscore/create', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ score: score }),
            })
            .then((response) => {
                if (response.ok) {
                    resolve();
                } else {
                    reject();
                }
            })
            .catch((error) => {
                console.error('Erreur lors de l\'enregistrement du score:', error);
                reject();
            });
        });
    }
});

